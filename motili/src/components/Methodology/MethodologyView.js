import React, { Component } from 'react';

import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';

import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';

import Img1 from './codereviews.jpg'
import Img2 from './prototyping.jpg'

import job from '../../job';

const methodData = [
  {
    title: 'Code Reviews',
    data: job.methodology.codereviews,
    image: Img1
  },
  {
    title: 'Prototyping',
    data: job.methodology.prototyping,
    image: Img2
  },
  {
    title: 'Pair Programming',
    data: job.methodology.pairprogramming,
    image: 'https://spin.atomicobject.com/wp-content/uploads/Pairing.jpg'
  },
  {
    title: 'Fail Fast',
    data: job.methodology.failfast,
    image: 'https://media.licdn.com/mpr/mpr/AAEAAQAAAAAAAAUeAAAAJGQ4MDRmMDhiLTNhNmYtNDkxYS04OGIzLWU3NDAwYzYyNDNjOA.jpg'
  },
  {
    title: 'Unit Testing',
    data: job.methodology.unittests,
    image: 'https://www.theschule.com/wp-content/uploads/2017/01/20150224test644.jpg'
  },
  {
    title: 'Integration Testing',
    data: job.methodology.integrationtests,
    image: 'http://www.alliedc.com/wp-content/uploads/2014/10/integration-testing-pic.jpg'
  },
  {
    title: 'Build Server',
    data: job.methodology.buildserver,
    image: 'https://msdnshared.blob.core.windows.net/media/2017/01/servers.jpg'
  },
  {
    title: 'Static Code Analysis',
    check: job.methodology.staticcodeanalysis,
    image: 'https://saas.hpe.com/sites/default/files/static-code-analysis-sast-513x289.jpg'
  },
  {
    title: 'Version Control',
    check: job.methodology.versioncontrol,
    image: 'https://dn-linuxcn.qbox.me/data/attachment/album/201611/29/175130yorx11s7oo4s9r7o.png'
  },
  {
    title: 'Issue Tracker',
    check: job.methodology.issuetracker,
    image: 'https://snaplogic-h.s3.amazonaws.com/uploads/snap/image/55/jira.jpg'
  },
  {
    title: 'Knowledge Repo',
    check: job.methodology.knowledgerepo,
    image: 'https://www.atlassian.com/docroot/wac/resources/wac/img/social-icons/confluence_logo.jpg'
  },
  {
    title: 'Standups',
    check: job.methodology.standups,
    image: 'https://martinfowler.com/articles/itsNotJustStandingUp/standingup.jpg'
  },
  {
    title: 'QA Protocol',
    check: job.methodology.qaprotocol,
    image: 'http://cdn.softwaretestinghelp.com/wp-content/qa/uploads/2011/09/Quality-Assurance.jpg'
  },
  {
    title: 'Freedom Over Tools',
    check: job.methodology.freedomovertools,
    image: 'https://www.memberpress.com/wp-content/uploads/2015/06/Google-tools@2x.png'
  },
  {
    title: 'One Command Build',
    check: job.methodology.onecommandbuild,
    image: 'http://previews.123rf.com/images/copacool/copacool1209/copacool120900061/15463269-Computer-keyboard-with-Build-key-business-concept-Stock-Vector.jpg'
  },
  {
    title: 'Quick Start',
    check: job.methodology.quickstart,
    image: 'http://www.dotworkz.com/wp-content/uploads/2015/10/quick-start-button.png'
  },
  {
    title: 'Commit On Day One',
    check: job.methodology.commitondayone,
    image: 'http://www.ethanjoachimeldridge.info/images/tech-blog/github.jpg'
  },
]

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: "100%",
    height: "30%",
    overflowY: 'auto',
  },
};


const MethodologyView = () => (
  <Card>

    <CardMedia
      overlay={<CardTitle title="Our Daily Methods"  overlayContainerStyle={{height: 10}} />}
      mediaStyle={{
        overflow: 'hidden', resizeMode:"cover", height: 400
      }}
    >
      <img src={require('./code.jpg')} alt="denver" className="essentials-img" />
    </CardMedia>
    <CardText>

      <GridList
      cellHeight={300}
      style={styles.gridList}
    >
      {methodData.map((method) => (
        <GridTile
          key={method.image}
          title={method.title}
          subtitle={method.check}
          cols={.66}
        >
          <img src={method.image} />
        </GridTile>
      ))}
    </GridList>

    </CardText>
  </Card>
)


export default MethodologyView
