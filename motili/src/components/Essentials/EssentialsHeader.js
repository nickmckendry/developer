import React, { Component } from 'react';

import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import Check from 'material-ui/svg-icons/navigation/check';
import Close from 'material-ui/svg-icons/navigation/close';


import job from '../../job';

const denver= job.essentials.locations

const EssentialsHeader = () => (
  <Card style={{
    height: 100
  }}>

    <CardMedia
      overlay={<CardTitle title="Location" subtitle={denver.toUpperCase()} overlayContainerStyle={{height: 10}} />}
      mediaStyle={{
        overflow: 'hidden', resizeMode:"cover", height: 400
      }}
    >
      <img
         src={require('./brick.jpg')}
         alt="denver"
         className="essentials-img"
         style={{overflow: 'hidden', resizeMode: 'cover'}}
     />
    </CardMedia>
    <CardTitle title="Employment" subtitle="Everything You Need to Know about working at Motili" style={{
      backgroundColor:"#232C33",
    }}
    titleStyle={{
      color:"#A0C1D1"
    }}
    subtitleStyle={{
      color:"#5A7D7C"
    }}
  />
    <CardText style={{
      display: "flex",
      flexDirection: 'row',
      justifyContent: 'space-between',
      backgroundColor: "#FFFFFF",
      color: "#232C33"
    }}>

    <div>
      <h3>Employment Status: {job.essentials.employment}</h3>
      <h3>Experience Types: {job.essentials.experience[0]} or {job.essentials.experience[1]}</h3>
      <h3>Start Date: {job.essentials.startdate}</h3>
      <h3>Company Size: {job.essentials.companysize}</h3>
      <h3>Team Size: {job.essentials.teamsize.min} - {job.essentials.teamsize.max}</h3>
      <h3>New Features: {job.profile.newfeatures}</h3>
      <h3>Documentation: {job.profile.documentation}</h3>
      <h3>Meetings: {job.profile.meetings}</h3>
    </div>

    <div>
      <h3>Workload: {job.specs.workload}</h3>
      <h3>Work Week: {job.specs.workweek} hours</h3>
      <h3>Core Hours: {job.specs.corehours.from} to {job.specs.corehours.to}</h3>
      <h3>Remote Work: {job.specs.remote}</h3>
      <h3>Paid Time Off: {job.specs.pto}</h3>
      <h3>Number of Clients: {job.profile.clientsupport}</h3>
      <h3>Maintenance: {job.profile.maintenance}</h3>
      <h3>Operating Sytems: {job.equipment.operatingsystem[0]} or {job.equipment.operatingsystem[1]}</h3>
    </div>

    <div>
      <h3>Training: {job.misc.training}</h3>
      <h3>Team Events: <Check /></h3>
      <h3>EcoPass: <Check /></h3>
      <h3>HealthCare: <Check /></h3>
      <h3>Dental: <Check /></h3>
      <h3>Mobile Phone: <Close /></h3>
      <h3>Kitchen: <Check /></h3>
      <h3>Free Stuff: {`${job.misc.freestuff[0]}, ${job.misc.freestuff[1]}, ${job.misc.freestuff[2]}, ${job.misc.freestuff[3]}`}</h3>
    </div>

    </CardText>
  </Card>
)


export default EssentialsHeader
