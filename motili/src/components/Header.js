import React, { Component } from 'react'

import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import Menu from 'material-ui/svg-icons/navigation/menu';
import {List, ListItem} from 'material-ui/List';
import injectTapEventPlugin from 'react-tap-event-plugin';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'



import job from '../job'

export default class Header extends Component {

  constructor(props){
    super(props)
    this.handleToggle = this.handleToggle.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.state = {open: false}
  }

  handleToggle = () => this.setState({ open: !this.state.open })

  handleClose = () => this.setState({ open: false })

  render(){
    return(
      <div>
        <AppBar
          title={job.headline}
          iconElementLeft={<IconButton><Menu /></IconButton>}
          onLeftIconButtonTouchTap={this.handleToggle}
          style={{
            backgroundColor: "#232C33",
            height: 80
          }}
          titleStyle={{
            color: "#A0C1D1"
          }}
        />

        <Drawer
          docked={false}
          width={200}
          open={this.state.open}
          onRequestChange={(open) => this.setState({open})}
          >
            <List>
              <ListItem
                primaryText="Essentials"
                containerElement={<Link to="/" />}
                linkButton={true}
              />
              <ListItem
                primaryText="Methodology"
                containerElement={<Link to="/methodology" />}
                linkButton={true}
              />
              <ListItem
                primaryText="Technologies"
                nestedItems={[
                  <ListItem
                    key={1}
                    primaryText="Junior"
                    containerElement={<Link to="/junior" />}
                    linkButton={true}
                  />,
                  <ListItem
                    key={2}
                    primaryText="Seasoned"
                    containerElement={<Link to="/seasoned" />}
                    linkButton={true}
                  />
                ]}
              />

            </List>
        </Drawer>
      </div>
    )
  }
}

injectTapEventPlugin()
