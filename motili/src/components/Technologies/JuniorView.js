import React, { component } from 'react';

import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';


import job from '../../job';


const JuniorView = () => (
  <Card style={{
  }}>
    <CardHeader
      title="Junior"
      style={{
        backgroundColor:"#232C33",
      }}

      titleStyle={{
        fontSize: "3em",
        textAlign: 'center',
        color:"#A0C1D1"

      }}
    />
    <Card style={{
    }}>
      <CardHeader
        title="Core Technologies"
        titleStyle={{
          fontSize: "1.5em",
          textAlign: 'center',
          color:"#232C33"
        }}
        style={{
          backgroundColor: "#B5B2C2"

        }}
        subtitle="Expected Levels Of Skill"
      />
      <CardText style={{
        display: "flex",
        flexDirection: "row",
        backgroundColor: "#DADFF7",
        color: "#232C33"

      }}>
        <List>
          <ListItem
            primaryText={`CSS3: ${job.technologies.junior.css3}`}
            leftIcon={<img src='https://images.duckduckgo.com/iu/?u=http%3A%2F%2Fio13-high-dpi.appspot.com%2Fimages%2FCSS3_Logo.svg&f=1' />} />
          </List>
          <List>
          <ListItem
            primaryText={`HTML: ${job.technologies.junior.html5}`}
            leftIcon={<img src='https://images.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2F6%2F61%2FHTML5_logo_and_wordmark.svg%2F1024px-HTML5_logo_and_wordmark.svg.png&f=1' />}
          />
        </List>
        <List>
          <ListItem
            primaryText={`JavaScript: ${job.technologies.junior.javascript}`}
            leftIcon={<img src='https://images.duckduckgo.com/iu/?u=https%3A%2F%2Fopenclipart.org%2Fimage%2F800px%2Fsvg_to_png%2F272343%2F1486640684.png&f=1' />}
          />
        </List>
        <List>
          <ListItem
            primaryText={`Design Concepts: ${job.technologies.junior.design}`}
            leftIcon={<img src='https://images.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2F7%2F73%2FSketch_logo_frame.svg%2F494px-Sketch_logo_frame.svg.png&f=1' />}
          />

        </List>
        <List>
          <ListItem
            primaryText={`Node: ${job.technologies.junior.node}`}
            leftIcon={<img src='https://images.duckduckgo.com/iu/?u=https%3A%2F%2Fcdn2.iconfinder.com%2Fdata%2Ficons%2Fnodejs-1%2F512%2Fnodejs-512.png&f=1' />}
          />
        </List>
        <List>
          <ListItem
            primaryText={`REST: ${job.technologies.junior.rest}`}
            leftIcon={<img src='https://images.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.drupal.org%2Ffiles%2Fproject-images%2Frain-drop-hi.png&f=1' />}
          />
        </List>
        <List>
          <ListItem
            primaryText={`UI/UX: ${job.technologies.junior.uiux}`}
            leftIcon={<img src='https://images.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2Ff%2Ffb%2FAdobe_Illustrator_CC_icon.svg%2F1000px-Adobe_Illustrator_CC_icon.svg.png&f=1' />}
          />
        </List>
      </CardText>
    </Card>
    <Card style={{
    }}>
      <CardHeader
        title="Testing"
        titleStyle={{
        fontSize: "1.5em",
        color:"#232C33"
      }}
      style={{
        backgroundColor: "#B5B2C2"
      }}
      subtitle="Know one of the options"
    />
    <CardText style={{
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-around",
      backgroundColor: "#DADFF7",
      color: "#232C33"
    }}>
      <List>
        <ListItem
          primaryText={`Junit: ${job.technologies.junior.testing.oneof.junit}`}
          leftIcon={<img src='https://images.duckduckgo.com/iu/?u=https%3A%2F%2Favatars1.githubusercontent.com%2Fu%2F874086%3Fv%3D3%26s%3D400&f=1' />}
        />
      </List>
      <List>
        <ListItem
          primaryText={`Mocha: ${job.technologies.junior.testing.oneof.mocha}`}
          leftIcon={<img src='https://images.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fen%2Fthumb%2F9%2F90%2FMocha_%2528JavaScript_framework%2529_%2528logo%2529.svg%2F1024px-Mocha_%2528JavaScript_framework%2529_%2528logo%2529.svg.png&f=1' />}
        />
      </List>
      <List>
        <ListItem
          primaryText={`Jasmine: ${job.technologies.junior.testing.oneof.jasmine}`}
          leftIcon={<img src='https://images.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fen%2Fthumb%2F2%2F22%2FLogo_jasmine.svg%2F1200px-Logo_jasmine.svg.png&f=1' />}
        />
      </List>
      <List>
        <ListItem
          primaryText={`Selenium: ${job.technologies.junior.testing.oneof.selenium}`}
          leftIcon={<img src='https://images.duckduckgo.com/iu/?u=https%3A%2F%2Fyt3.ggpht.com%2F-r8IAK02syjk%2FAAAAAAAAAAI%2FAAAAAAAAAAA%2F7-iGZNygfP8%2Fs900-c-k-no%2Fphoto.jpg&f=1' />}
        />
      </List>
    </CardText>
    </Card>
    <Card style={{
    }}>
      <CardHeader
        title="Frameworks"
        titleStyle={{
        fontSize: "1.5em",
        color:"#232C33"
      }}
      style={{
        backgroundColor: "#B5B2C2"
      }}
      subtitle="Know one of the options"
    />
    <CardText style={{
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-around",
      backgroundColor: "#DADFF7",
      color: "#232C33"
    }}>
      <List>
        <ListItem
          primaryText={`React: ${job.technologies.junior.framework.oneof.react}`}
          leftIcon={<img src='https://images.duckduckgo.com/iu/?u=https%3A%2F%2Fcdn4.iconfinder.com%2Fdata%2Ficons%2Flogos-3%2F600%2FReact.js_logo-512.png&f=1' />}
        />
      </List>
      <List>
        <ListItem
          primaryText={`Vue: ${job.technologies.junior.framework.oneof.vue}`}
          leftIcon={<img src='https://images.duckduckgo.com/iu/?u=https%3A%2F%2Fcosmicjs.com%2Fuploads%2F96da6390-e18a-11e6-a19e-716cc90a0c51-Vue.js_Logo.svg&f=1' />}
        />
      </List>
      <List>
        <ListItem
          primaryText={`Angular: ${job.technologies.junior.framework.oneof.angular}`}
          leftIcon={<img src='https://images.duckduckgo.com/iu/?u=https%3A%2F%2Fthink360studio.com%2Fwp-content%2Fuploads%2F2016%2F03%2Fangular-js-icon.svg&f=1' />}
        />
      </List>

    </CardText>
    </Card>
    <Card style={{
    }}>
      <CardHeader
        title="Extras"
        titleStyle={{
        fontSize: "1.5em",
        color:"#232C33"
      }}
      style={{
        backgroundColor: "#B5B2C2"
      }}
      subtitle="Bonus Point Skills"
    />
    <CardText style={{
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-around",
      backgroundColor: "#DADFF7",
      color: "#232C33"
    }}>
      <List>
        <ListItem>Board Games: {job.technologies.junior.boardgames}</ListItem>
      </List>
      <List>
        <ListItem>DevOps: {job.bonuspoints.devops}</ListItem>
      </List>
      <List>
          <ListItem>SQL: {job.bonuspoints.sql}</ListItem>
      </List>
      <List>
        <ListItem>Mobile Development: {job.bonuspoints.mobiledevelopment}</ListItem>
      </List>
      <List>
        <ListItem>Quoting Bad Action Movies: {job.bonuspoints.quotingbadactionmovies}</ListItem>
      </List>
    </CardText>
    </Card>
  </Card>
)

export default JuniorView
