import React, { Component } from 'react';
import './App.css';

import job from "./job"
import Header from "./components/Header"
import MethodologyView from "./components/Methodology/MethodologyView"

class Methodology extends Component {
  render() {

    console.log(job.headline);

    return (
      <div>
        <Header />
        <MethodologyView />
      </div>
    );
  }
}

export default Methodology;
