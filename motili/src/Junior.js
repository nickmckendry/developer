import React, { Component } from 'react';

import JuniorView from './components/Technologies/JuniorView'
import Header from './components/Header'

export default class Junior extends Component {
  render(){
    return(
      <div>
        <Header />
        <JuniorView />
      </div>
    )
  }
}
