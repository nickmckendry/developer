import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import Essentials from './Essentials';
import Methodology from './Methodology'
import Profile from './Profile'
import Junior from './Junior'
import Seasoned from './Seasoned'

import EssentialsHeader from "./components/Essentials/EssentialsHeader"
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'


ReactDOM.render(
  <Router>
    <MuiThemeProvider>
      <div>
        <Route exact path='/' component={Essentials} />
        <Route path='/methodology' component={Methodology} />
        <Route path='/profile' component={Profile} />
        <Route path='/junior' component={Junior} />
        <Route path='/seasoned' component={Seasoned} />
      </div>
    </MuiThemeProvider>
  </Router>

  ,
  document.getElementById('root')
);



if(module.hot) {
  module.hot.accept()
}
