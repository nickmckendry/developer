import React, { Component } from 'react';

import SeasonedView from './components/Technologies/SeasonedView'
import Header from './components/Header'

export default class Junior extends Component {
  render(){
    return(
      <div>
        <Header />
        <SeasonedView />
      </div>
    )
  }
}
